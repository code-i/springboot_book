
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL COMMENT '主键',
  `pass_word` varchar(32) DEFAULT NULL COMMENT '密码',
  `user_name` varchar(32) DEFAULT NULL COMMENT '用户名',
   PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户表';

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL COMMENT '主键',
  `role_name` varchar(128) DEFAULT NULL COMMENT '角色名称',
   PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL COMMENT '主键',
  `menu_name` varchar(128) DEFAULT NULL COMMENT '菜单名称',
   PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单角色表';

CREATE TABLE `user_role` (
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `role_id` int(11) DEFAULT NULL COMMENT '角色id',
   KEY `user_role_id` (`user_id`,`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色关系表';

CREATE TABLE `role_menu` (
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `menu_id` int(11) DEFAULT NULL COMMENT '菜单id',
   KEY `role_menu_id` (`role_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色和菜单关系表';

